
typedef struct Fleche Fleche;
struct Fleche {

	int tir;
	int posX_fleche;
	int posY_fleche;
	int direction;
	int compteur;
	Fleche *suivant;

};

typedef struct premiere_Fleche premiere_Fleche;
struct premiere_Fleche  {

	Fleche *premier;

};


premiere_Fleche *initialisation();
void insertion(premiere_Fleche *premiere_fleche, int nv_tir, int nv_posX_fleche, int nv_posY_fleche, int nv_direction,int nv_compteur);
void suppression(premiere_Fleche *premiere_fleche);
void afficherListe(premiere_Fleche *premiere_fleche);
int tir_Fleche(premiere_Fleche *premiere_fleche, int nb_fleches, int *posX_d, int *posY_d);

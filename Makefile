CC=gcc
CFLAGS=-Wall -O0 -lncurses
EXEC=towerfall

CWD := $(shell pwd)

all: $(EXEC) 
	@echo "Lancement du makeflie"
	@echo ""
	@echo "          cwd: $(CWD)"
	@echo ""
	@echo "Le fichier executable est : towerfall"

towerfall: main.c creamap.c creamap.h menu.c menu.h init.c init.h deplacements.c deplacements.h version_tir_fleche2.c version_tir_fleche2.h	
	$(CC) $(CFLAGS) main.c creamap.c creamap.h menu.c menu.h init.c init.h deplacements.c deplacements.h version_tir_fleche2.c version_tir_fleche2.h -o $(EXEC)
	./$(EXEC) 
dist: 
	cd $(CWD) && tar -czvf towerfile.tar.gz *
clean:
	rm -f *.o *.tar.gz core
mrpropre: clean
	rm -f $(EXEC) *.txt



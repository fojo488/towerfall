#include <time.h>
#include <stdlib.h>  
#include <stdio.h>  /* Pour EXIT_SUCCESS */   
#include <unistd.h>
#include <sys/types.h>

#include <ncurses.h>    /* Pour printw, attron, attroff, COLOR_PAIR, getch */

#include "creamap.h"


/**
 * Initialisation de la souris.
 */
void ncurses_souris() {
  ncurses_initialiser();  
  if(!mousemask(ALL_MOUSE_EVENTS, NULL)) {
    ncurses_stopper();
    fprintf(stderr, "Erreur lors de l'initialisation de la souris.\n");
    exit(EXIT_FAILURE);
  }
 
  if(has_mouse() != TRUE) {
    ncurses_stopper();
    fprintf(stderr, "Aucune souris n'est détectée.\n");
    exit(EXIT_FAILURE);
  }
}
 
/**
 * Recupere la position x et y de la souris.
 * @param[out] x la position en x
 * @param[out] y la position en y
 * @param[out] bouton l'évenement associé au clic
 * @return OK si reussite
 */
int souris_getpos(int *x, int *y, int *bouton) {
  ncurses_initialiser();
  MEVENT event;
  int resultat = getmouse(&event);
 
  if(resultat == OK) {
    *x = event.x;
    *y = event.y;
    *bouton = event.bstate;
  }
  return resultat;
}


FILE* score = NULL;
int affi_score() {

char object,tab[500][500];
int ch;
  /* Initialisation de ncurses */
  ncurses_initialiser();
  ncurses_souris();
  scrollok(stdscr, TRUE);



score=fopen("MAPS/score.txt","r");

for(int y=0;y<LINES;y++) {
	for(int i=0;i<COLS;i++)
		{
		object=fgetc(score);
		if(object==',')tab[y][i]=' ';
		else tab[y][i]=object;
		 
		}
		fscanf(score,"\n");

		
		}
fclose(score);

for(int y=0;y<LINES;y++)
{
	for(int i=0;i<COLS;i++)
		{	
		mvaddch(y, i, tab[y][i]);
						
		}
}
while((ch = getch()) != KEY_F(2)) {}
/* Arrêt de ncurses */
  ncurses_stopper();
return 0;
}


FILE* data=NULL;

 



int crea_map() {
  int sourisX, sourisY, bouton, ch, PosX,PosY, ValX=1, ValY=1, Efface=0;
  char Tab[256][256];


  /* Initialisation de ncurses */
  ncurses_initialiser();
  ncurses_souris();
  scrollok(stdscr, TRUE);

//initialisation du tableau en full virgule 
  for (int i=0; i<LINES; i++){
	for (int j=0; j<COLS; j++){
		mvaddch(i, j, ',');

		Tab[i][j]=',';
	}
}
 
  /* Routine principale */
  while((ch = getch()) != KEY_F(2)) {
    
      
//////on passe en mode effacer 	
      if (ch == 'p'){
	Efface=1;
	ValY=1;
	ValX=1;
	}

//////on passe en mode ecriture
      if (ch == 'o'){
	Efface=0;	
	}

//////choix de la taille des blocs 
      if (ch == 'a'){
	ValY = 4;
	ValX = 4;
	}
      if (ch == 'z'){
	ValY = 2;
	ValX = 2;
	}
      if (ch == 'e'){
	ValY = 1;
	ValX = 1;
	
	}
//case KEY_MOUSE:
        if(souris_getpos(&sourisX, &sourisY, &bouton) == OK) {

/////////mode ecriture (de base on est dans ce mode )
	 if(Efface == 0 ){ 
	  if(bouton & BUTTON1_DOUBLE_CLICKED){
		for (int i=0; i<COLS; i++) {
			Tab[sourisY][i] = '%';
			attron(COLOR_PAIR(3));
			mvaddch(sourisY, i, '%');
			attroff(COLOR_PAIR(3));}	    	
		}
	 

	  if (bouton & BUTTON1_CLICKED){
	    for (int i=0; i<ValX;i++){
		for (int j=0; j<ValY; j++){
			
			PosX= sourisX +j;
			PosY= sourisY +i ;
			Tab[PosY][PosX]= '%';
			attron(COLOR_PAIR(3));
			mvaddch(PosY , PosX, '%');
			attroff(COLOR_PAIR(3));
			}}}}
	




////////mode effacer 
	if(Efface == 1){ 
	  if(bouton & BUTTON1_DOUBLE_CLICKED){
		for (int i=0; i<COLS; i++) {
			Tab[sourisY][i] = ',';
			attron(COLOR_PAIR(2));	
			mvaddch(sourisY, i, ',');
			attroff(COLOR_PAIR(2));}	    	
		}
	 

	  if (bouton & BUTTON1_CLICKED){
	    for (int i=0; i<ValX;i++){
		for (int j=0; j<ValY; j++){
			PosX= sourisX +j;
			PosY= sourisY +i;
			Tab[PosY][PosX]= ',';
			attron(COLOR_PAIR(2));
			mvaddch(PosY, PosX, ',');
			attroff(COLOR_PAIR(2));
			}}}}
	  
    	   
	  refresh();
	}
	
    
  }
 
 
  /* Arrêt de ncurses */
  ncurses_stopper();


//ecriture du tableau dans le fichier texte 
  data = fopen("DATA.txt","w");
    for (int i=0; i<LINES; i++){
	for (int j=0; j<COLS; j++){
		fputc(Tab[i][j], data);
	}
	fprintf(data,"\n");
	}
 fclose(data);
 

  return 1;
}

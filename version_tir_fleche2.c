#include <stdlib.h>     
#include <ncurses.h>    
#include <unistd.h>
#include <sys/types.h>


//gcc Projet_S2_0.2.c -lncurses -o exe

//gcc version_tir_fleche2.c -lncurses -o exe

#include "version_tir_fleche2.h"

premiere_Fleche *initialisation() {
	premiere_Fleche *premiere_fleche = malloc(sizeof(*premiere_fleche));
	Fleche *fleche = malloc(sizeof(*fleche));

	if (premiere_fleche == NULL || fleche == NULL)
	{
		exit(EXIT_FAILURE);
	}

	fleche->tir = 0;
	fleche->posX_fleche = 0;
	fleche->posY_fleche = 0;
	fleche->direction = 0;
	fleche->compteur = 0;		
	fleche->suivant = NULL;
	premiere_fleche->premier = fleche;

	return premiere_fleche;
}

void insertion(premiere_Fleche *premiere_fleche, int nv_tir, int nv_posX_fleche, int nv_posY_fleche, int nv_direction,int nv_compteur)//nvNombre le nombre que l'on ajoute dans la liste dans l'exemple

{
	/* Création du nouvel élément */
	Fleche *nouveau = malloc(sizeof(*nouveau));
	if (premiere_fleche == NULL || nouveau == NULL)
	{
		exit(EXIT_FAILURE);
	}
	nouveau->tir = nv_tir;
	nouveau->posX_fleche = nv_posX_fleche;
	nouveau->posY_fleche= nv_posY_fleche;
	nouveau->direction = nv_direction;
	nouveau->compteur = nv_compteur;

	/* Insertion de l'élément au début de la liste */
	nouveau->suivant = premiere_fleche->premier;
	premiere_fleche->premier = nouveau;
}

void suppression(premiere_Fleche *premiere_fleche) {
	if (premiere_fleche == NULL) {
		exit(EXIT_FAILURE);
	}

	if (premiere_fleche->premier != NULL)	{
		Fleche *aSupprimer = premiere_fleche->premier;
		premiere_fleche->premier = premiere_fleche->premier->suivant;
		free(aSupprimer);
	}
}


void afficherListe(premiere_Fleche *premiere_fleche) {
	if (premiere_fleche == NULL) {
		exit(EXIT_FAILURE);
	}

	Fleche *actuel = premiere_fleche->premier;

	while (actuel != NULL) 	{
		printf("Actuel tir : %d -> ", actuel->tir);
		printf("PosX : %d -> ", actuel->posX_fleche);
		printf("PosY : %d -> ", actuel->posY_fleche);
		printf("Actuel direction : %d -> \n", actuel->direction);
		actuel = actuel->suivant;
	}
	printf("NULL\n");
}

int tir_Fleche(premiere_Fleche *premiere_fleche, int nb_fleches,int *posX_d,int *posY_d) {
	int carquois = nb_fleches;
	int sens,aim,posX,posY,tir;
	Fleche *actuel = premiere_fleche->premier;
	

		while (actuel != NULL) {
			actuel->compteur=actuel->compteur+1;
			if((actuel->direction==6 && mvinch(actuel->posY_fleche, actuel->posX_fleche+2) != ' ') || (actuel->direction==4 && mvinch(actuel->posY_fleche, actuel->posX_fleche-2) != ' ') || (actuel->direction==8 && mvinch(actuel->posY_fleche-3, actuel->posX_fleche) == '%')) {
				//suppression(actuel);
				actuel->direction=10;
				mvaddch(actuel->posY_fleche, actuel->posX_fleche, ' ');
				*posY_d=actuel->posY_fleche;
			        *posX_d=actuel->posX_fleche;
				
				//actuel = actuel->suivant;

			}
			

				switch(actuel->direction) {
					case 6:
						
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche+4, '>');
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche+3, '-');
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche+2, 'x');
						if(actuel->compteur%50 == 0) actuel->posX_fleche++;
						
						break;
					case 4:
						
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche-4, '<');
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche-3, '-');
						mvaddch(actuel->posY_fleche-1, actuel->posX_fleche-2, 'x');
						if(actuel->compteur%50 == 0) actuel->posX_fleche--;
						
						break;
					case 8:
						
						mvaddch(actuel->posY_fleche-6, actuel->posX_fleche, '^');
						mvaddch(actuel->posY_fleche-5, actuel->posX_fleche, '|');
						mvaddch(actuel->posY_fleche-4, actuel->posX_fleche, 'x');
						if(actuel->compteur%50 == 0) actuel->posY_fleche--;
						
						break;
					/*
					case 9:
						
						mvaddch(actuel->posY_fleche-6, actuel->posX_fleche+4, '^');
						mvaddch(actuel->posY_fleche-5, actuel->posX_fleche+3, '/');
						mvaddch(actuel->posY_fleche-4, actuel->posX_fleche+2, 'x');
						if(*compteur%250 == 0){ 

							actuel->posX_fleche++;
							actuel->posY_fleche--;}
						break;
					case 5:
						
						mvaddch(actuel->posY_fleche+6, actuel->posX_fleche, 'v');
						mvaddch(actuel->posY_fleche+5, actuel->posX_fleche, '|');
						mvaddch(actuel->posY_fleche+4, actuel->posX_fleche, 'x');
						if(*compteur%250 == 0){ 

							actuel->posY_fleche++;
						}
						break;
					case 7:
						
						mvaddch(actuel->posY_fleche+6, actuel->posX_fleche+4, '^');
						mvaddch(actuel->posY_fleche+5, actuel->posX_fleche+3, '\\');
						mvaddch(actuel->posY_fleche+4, actuel->posX_fleche+2, 'x');
						if(*compteur%250 == 0){ 

							actuel->posX_fleche--;
							actuel->posY_fleche--;}
						break;
					case 1:
						
						mvaddch(actuel->posY_fleche-6, actuel->posX_fleche+4, 'x');
						mvaddch(actuel->posY_fleche-5, actuel->posX_fleche+3, '/');
						mvaddch(actuel->posY_fleche-4, actuel->posX_fleche+2, 'v');
						if(*compteur%250 == 0){ 

							actuel->posX_fleche++;
							actuel->posY_fleche--;}
						break;
					case 3:
						
						mvaddch(actuel->posY_fleche+6, actuel->posX_fleche-4, 'x');
						mvaddch(actuel->posY_fleche+5, actuel->posX_fleche-3, '\\');
						mvaddch(actuel->posY_fleche+4, actuel->posX_fleche-2, 'v');
						if(*compteur%250 == 0){ 

							actuel->posX_fleche++;
							actuel->posY_fleche--;}
						
						break;
					*/

				
				}
			actuel = actuel->suivant;
			}

return 0;
}



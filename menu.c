#include <stdlib.h>     /* Pour EXIT_SUCCESS */
#include <ncurses.h>    /* Pour printw, attron, attroff, COLOR_PAIR, getch */
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <sys/time.h> 
#include <stdio.h>  

#include "deplacements.h"
#include "init.h"
#include "menu.h"
#include "creamap.h"

void joueur(){
initscr();
WINDOW *text ;
text = newwin (5,25,COLS/2,LINES/2);
box(text, ACS_VLINE, ACS_HLINE);
}


void ViderBuffer()
{
	int c=0;
	while (c!='\n' && c!= EOF){c=getchar();}
}

int menue() 
{
  for (int i=0; i<LINES; i++){
	for (int j=0; j<COLS; j++){
		mvaddch(i, j, ' ');
	}
}
WINDOW *w;
char list[5][10] = { " Play ", " CreaMap ", " Credits ", " -(;D)- ", " EXIT "};
char item[10];
int ch, i=0, width=7, entree=10,l;
initscr();

int tic_time(struct timeval t2, int t_temp,int delay){

	struct timeval t1;
	double elapsedTime;
	gettimeofday(&t1, NULL);
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;		
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
	int elapsed = (int)elapsedTime;
	return elapsed;
}

void affiche_joueur(int sens_deplacement,int posX,int posY) {
	if(sens_deplacement == 4){
		mvaddch(posY-2, posX, 'd');
		mvaddch(posY-1, posX-2, '{');
		mvaddch(posY-1, posX-1, '-');
		mvaddch(posY-1, posX+1, '\\');
	}
	if(sens_deplacement == 6){
		mvaddch(posY-2, posX, 'b');
		mvaddch(posY-1, posX+2, '}');
		mvaddch(posY-1, posX+1, '-');
		mvaddch(posY-1, posX-1, '/');
	}
	mvaddch(posY-1, posX, '|');
	mvaddch(posY, posX, 'n');
}

int gravite (int posX,int posY,int delay,int t_temp){

	if(t_temp%delay == 0) {
		if(mvinch(posY+1, posX)==' ') posY++;
	}
	return posY;
}

int decalage_player(int posX,int posY,int delay,int t_temp){
	if(t_temp%delay == 0) posX--;
	return posX;
}

char * map_generation(int sns, int firt)
{
	FILE* fichier = NULL;
	FILE* fichier2 = NULL;
	FILE* data = NULL;
	FILE* data2 = NULL;

	char object;
	static char tab[500][500]={0};

	data2=fopen("MAPS/save_temp.txt","r+");

	if(firt==1) {data=fopen("MAPS/save1.txt","r");}
	if(firt==2) {data=fopen("MAPS/save2.txt","r");}
	if(firt==3) {data=fopen("MAPS/save3.txt","r");}
	if(firt==4) {data=fopen("MAPS/save4.txt","r");}

	if(firt==0) {
		data=fopen("MAPS/save1.txt","r");
		fichier=fopen("MAPS/save0.txt","r");
		fichier2=fopen("MAPS/save00.txt","r");
	}
	else {
		fichier=data2;
		fichier2=data;
	}


	for(int y=0;y<LINES;y++) {
		for(int i =0;i<sns;i++) {object=fgetc(fichier);}
		for(int i=0;i<COLS-sns;i++)
		{
			object=fgetc(fichier);
			if(object==',')tab[y][i]=' ';
			else tab[y][i]=object;
		}
		for(int i=COLS-sns;i<COLS;i++)
		{

			object=fgetc(fichier2);
			if(object==',')tab[y][i]=' ';
			else tab[y][i]=object;
		}
		for(int i =0;i<COLS-sns;i++) {object=fgetc(fichier2);}
		fscanf(fichier,"\n",&object);
		fscanf(fichier2,"\n",&object);
	}

	if(sns==210)
	{
		fseek(fichier2, 0, SEEK_SET);fseek(data2, 0, SEEK_SET);
		for(int y=0;y<LINES;y++) {
			for(int i=0;i<COLS;i++)
			{
				object=fgetc(fichier2);
				fputc(object,data2);
			}
			fscanf(fichier2,"\n",&object);
			fprintf(data2,"\n",&object);
		}
	}

	if (firt==0){fclose(fichier); fclose(fichier2);}  
	fclose(data);
	fclose(data2);
	return tab;
}

int changement_map_alea(int sns,int firt,time_t t)
{
	srand((unsigned) time(&t));
	if(sns==0)
	{
		firt=(rand()%4)+1;
	}
	return firt;
}

int decalage_map(int sns,int delay,int t_temp){
	if(t_temp%delay == 0) sns++;
	if(sns==211) sns=0;
	return sns;
}

void casse_mur(int *posY_temp,int posX_d,int posY_d,int t_temp,int *z){
	if(posX_d!=*posY_temp) {
		*z=0;
		*posY_temp=posX_d;
	}
	mvaddch(posY_d-1, posX_d-*z+5, ' ');
	mvaddch(posY_d-2, posX_d-*z+5, ' ');
	mvaddch(posY_d-3, posX_d-*z+5, ' ');
	mvaddch(posY_d+1, posX_d-*z+5, ' ');
	mvaddch(posY_d, posX_d-*z+5, ' ');
	mvaddch(posY_d-1, posX_d-*z+3, ' ');
	mvaddch(posY_d-2, posX_d-*z+3, ' ');
	mvaddch(posY_d-3, posX_d-*z+3, ' ');
	mvaddch(posY_d+1, posX_d-*z+3, ' ');
	mvaddch(posY_d, posX_d-*z+3, ' ');
	mvaddch(posY_d-1, posX_d-*z+4, ' ');
	mvaddch(posY_d-2, posX_d-*z+4, ' ');
	mvaddch(posY_d-3, posX_d-*z+4, ' ');
	mvaddch(posY_d+1, posX_d-*z+4, ' ');
	mvaddch(posY_d, posX_d-*z+4, ' ');

	mvaddch(posY_d-1, posX_d-*z+1, ' ');
	mvaddch(posY_d-2, posX_d-*z+1, ' ');
	mvaddch(posY_d-3, posX_d-*z+1, ' ');
	mvaddch(posY_d+1, posX_d-*z+1, ' ');
	mvaddch(posY_d, posX_d-*z+1, ' ');
	mvaddch(posY_d-1, posX_d-*z+2, ' ');
	mvaddch(posY_d-2, posX_d-*z+2, ' ');
	mvaddch(posY_d-3, posX_d-*z+2, ' ');
	mvaddch(posY_d+1, posX_d-*z+2, ' ');
	mvaddch(posY_d, posX_d-*z+2, ' ');
	mvaddch(posY_d-1, posX_d-*z, ' ');
	mvaddch(posY_d-2, posX_d-*z, ' ');
	mvaddch(posY_d-3, posX_d-*z, ' ');
	mvaddch(posY_d+1, posX_d-*z, ' ');
	mvaddch(posY_d, posX_d-*z, ' ');

	if((t_temp%200) == 0) {
		*z+=1;
	}
}



//fenêtre
w=newwin(LINES/2, COLS-1, LINES/4, 1);
box(w, ACS_VLINE, ACS_HLINE);
mvprintw( LINES/4-1, (COLS / 2 +1) - (strlen(list[i]) / 2), "%s", "WELCOME !" );

/* Routine principale */
for( i=0; i<5; i++ )
{
    if( i == 0 ) 
    {wattron( w, A_STANDOUT );} // highlights the first item.
    else
    {wattroff( w, A_STANDOUT );}
    
    sprintf(item, "%-7s",  list[i]);
    mvwprintw( w, (LINES/12)*i+2, (COLS / 2) - (strlen(list[i]) / 2), "%s", item );
}

refresh(); // update the terminal screen
i = 0;
noecho();
keypad(w, TRUE);
curs_set(0);

while((ch = wgetch(w)) != entree)
{
   sprintf(item, "%-7s", list[i]);
   mvwprintw(w, (LINES/12)*i+2, (COLS / 2) - (strlen(list[i]) / 2), "%s", item);
    
    switch(ch)
    {
        case KEY_UP:
        i--;
        i = (i<0) ? 4 : i;
        break;
        case KEY_DOWN:
        i++;
        i = ( i>4 ) ? 0 : i;
        break;
	ViderBuffer();
    }
wattron(w, A_STANDOUT);
sprintf(item, "%-7s", list[i]);
mvwprintw(w, (LINES/12)*i+2, (COLS / 2) - (strlen(list[i]) / 2), "%s", item);
wattroff(w, A_STANDOUT);

} 

delwin(w);
endwin();

	
//on met les conditions sur le i selon ce qu'on veut selectionner, et y associer le code correspondant.
//ça va se faire avec les .h et .c nrmlt
 
if(i==0){
char *p;
	char *p_trou;
	int sns=0;
	int posX_temp,posY_temp=100;
	int delay=200;
	int delay_grav=200;
	int firt = 0;
	int ch, posX, posY, posX_f, posY_f,posX_d=300,posY_d=300,tir=0, compteur = 0, first, aim, sens=6;
	int saut = 0,sautOn = 0,ndsaut=0,compteurFleche=0;
	char object;

	int z=0;

	time_t t;
	ncurses_initialiser();
	posX = COLS / 2 -1;
	posY = LINES / 2 - 1;
	premiere_Fleche *maFleche = initialisation();


	struct timeval t2;
	struct timeval t3;
	struct timeval t4;
	int t_temp=0,t_temp_grav=0;unsigned long int score=0;int multiplicateur=1;
	gettimeofday(&t2, NULL);
	gettimeofday(&t3, NULL);
	gettimeofday(&t4, NULL);


	while((ch = getch()) != KEY_F(2)) {
		t_temp=tic_time(t2,t_temp,delay);
		p=map_generation(sns,firt);
		firt=changement_map_alea(sns,firt,t);
		sns=decalage_map(sns,delay,t_temp);


		for(int y=0;y<LINES;y++)
		{
			for(int i=0;i<COLS;i++)
			{	
				mvaddch(y, i,' ');
			}
		}

		for(int y=0;y<LINES;y++)
		{
			for(int i=0;i<COLS;i++)
			{	
				mvaddch(y, i, *(p+(y*500)+i));

			}
		}


		casse_mur(&posY_temp,posX_d,posY_d,t_temp,&z);
		tir_Fleche(maFleche,0,&posX_d,&posY_d);

		posY=gravite(posX,posY,delay_grav,t_temp_grav);
		t_temp_grav=tic_time(t3,t_temp_grav,delay);
		commande(maFleche,&ch,&tir,LINES,COLS,&sens,&aim,&posX,&posY,&sautOn,&saut,&first,&compteurFleche);
		deplacement(&compteur,&sens, &posX, &posY,&sautOn, &saut,&ndsaut,&delay_grav,&t_temp_grav);
		posX=decalage_player(posX,posY,delay,t_temp);

		affiche_joueur(sens,posX,posY);

		move(0,0);
		printw("temp = %d et z =%d",t_temp,z);


		if(posX < 0) break;
		if(posY > LINES-2) break;

		refresh();
	}
}
else if(i==1){crea_map();}
else if(i==2){affi_score();}
if(i==3){crea_map();}


return i;


}

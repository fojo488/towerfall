#include "deplacements.h"

void deplacement(int *compteur,int *sens, int *posX, int *posY,int (*sautOn), int *saut,int *ndsaut,int *delay_grav,int  *t_temp_grav)
{
	//gestion des sauts
  
	if((*sautOn)>0 && *t_temp_grav%75 == 0 && mvinch(*posY-1, *posX)==' ' && (*saut<5 || *ndsaut<5))
	{
		if ((*sautOn)==1 && (*saut)<5){(*posY)--; (*saut)++;}
		else if ((*sautOn)==2 && (*ndsaut)<5){(*posY)--; (*ndsaut)++;}

		//déplacement en vol
		if(*t_temp_grav%75 == 0)
		{
			if(*sens==6 && mvinch(*posY, *posX+1)==' ' ){(*posX)++;}
			else if(*sens==4 && mvinch(*posY, *posX-1)==' '){(*posX)--;}
		}
		
	}
	//déplacement en vol
	if(*t_temp_grav%*delay_grav == 0 && mvinch(*posY+1, *posX)==' ')
	{
		if(*sens==6 && mvinch(*posY, *posX+1)==' ' ){(*posX)++;}
		else if(*sens==4 && mvinch(*posY, *posX-1)==' '){(*posX)--;}
	}

	if(mvinch(*posY+1, *posX)!=' ' && (*sautOn)>0 && ((*saut)==5 || (*ndsaut)==5)){(*sautOn)=0;(*saut)=0;(*ndsaut)=0;}
}

void commande(premiere_Fleche *maFleche,int *ch,int *tir,int LINES,int COLS, int *sens, int *aim, int *posX, int *posY,int (*sautOn), int *saut,int *first,int *compteurFleche)
{
//int ch = getch();
	switch(*ch) {
      case '4'://gauche

	if(*posX > 0 && mvinch(*posY, *posX-1)==' ') (*posX)--; if(*tir==0) *aim=4;
	*sens=4;
	break;
      case '6'://droite
	if(*posX < COLS - 1 && mvinch(*posY, *posX+1)==' ') (*posX)++; if (*tir==0) *aim=6;
        *sens=6;
	break;
      case '8':
	*aim=8;
	break;
/*
      case '5':
	if(*posY < LINES - 1 && mvinch(*posY+1, *posX)==' ') (*posY)++;
	break;
*/
      case '9':
	if(*posX < COLS - 1 && mvinch(*posY, *posX+1)==' ') (*posX)++;
	if(*posY > 0 && mvinch(*posY-1, *posX)==' ') (*posY)--;
	break;
      case '7':
	if(*posX > 0 && mvinch(*posY, *posX-1)==' ') (*posX)--;
	if(*posY > 0 && mvinch(*posY-1, *posX)==' ') (*posY)--;
	break;
      case '1':
	if(*posX > 0 && mvinch(*posY, *posX-1)==' ') (*posX)--;
	if(*posY < LINES - 1 && mvinch(*posY+1, *posX)==' ') (*posY)++;
	break;
      case '3':
	if(*posY < LINES - 1 && mvinch(*posY+1, *posX)==' ') (*posY)++;
	if(*posX < COLS - 1 && mvinch(*posY, *posX+1)==' ') (*posX)++;
	break;
      case ' '://saut
	if((*sautOn)==0) (*sautOn)=1; //le personnage se met à sauter
	if((*sautOn)==1 && *saut>1) (*sautOn)=2; //le personnage se met à 'double' sauter
	break;
      case 'n'://tir
        insertion(maFleche,*compteurFleche,*posX,*posY,*aim,0);
	(*compteurFleche)++;
        break;
	   }
	
}

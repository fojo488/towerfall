//gcc Projet_S2_0.1.c -lncurses -o exe

//gcc source.c version_tir_fleche2.c version_tir_fleche2.h -lncurses -o exe

//gcc source.c version_tir_fleche2.c version_tir_fleche2.h deplacements.c deplacements.h init.c init.h -lncurses && ./a.out


#include <stdlib.h>     /* Pour EXIT_SUCCESS */
#include <ncurses.h>    /* Pour printw, attron, attroff, COLOR_PAIR, getch */
#include <unistd.h>
#include <sys/types.h>

#include "deplacements.h"
#include "init.h"
/**
 * Initialisation de ncurses.
 */

int main() {
  int ch, posX, posY, posX_map, posY_map, posX_f, posY_f, tir=0, compteur = 0, first, aim, sens=6,saut = 0,sautOn = 0,ndsaut=0,compteurFleche=0;
  premiere_Fleche *maFleche = initialisation();
  /* Initialisation de ncurses */
  ncurses_initialiser();
  printw("Pressez F2 pour quitter le programme. Utilisez les flèches pour déplacer le curseur.\n");
 
  /* Place le curseur à la position de départ */
  posX = COLS / 2 -1;
  posY = LINES / 2 - 1;
  posX_map=posX;
  posY_map=posY;

  posY--;
  posX++;
  mvaddch(posY, posX, 'O');
  refresh();
	
posX_f = posX+1; posY_f=posY;
	  /* Routine principale */
  while((ch = getch()) != KEY_F(2)) {
    /* On efface le curseur */

//generation map
for(int y=0;y<LINES;y++)
{
    for(int i=0;i<COLS;i++)
    {
        mvaddch(y, i, ' ');
    }
}

//deplacment map
	compteur +=1;
	if(compteur%5000 == 0) {posX-=1; posX_map-=1;} 
	if(posX < 0) posX++;

 
    
    mvaddch(posY_map-1, posX_map+25, 'x');
    mvaddch(posY_map-2, posX_map+25, 'x');
    mvaddch(posY_map-3, posX_map+25, 'x');
    mvaddch(LINES-6, posX_map+150, 'x');
    mvaddch(LINES-7, posX_map+150, 'x');
    mvaddch(LINES-8, posX_map+150, 'x');
    mvaddch(LINES-6, posX_map+1, 'x');
    mvaddch(LINES-7, posX_map+1, 'x');
    mvaddch(LINES-8, posX_map+1, 'x');
    mvaddch(LINES-11, posX_map+4, 'x');
    mvaddch(LINES-11, posX_map+5, 'x');
    mvaddch(LINES-11, posX_map+6, 'x');
    mvaddch(LINES-11, posX_map+7, 'x');
    mvaddch(LINES-11, posX_map+8, 'x');
    mvaddch(LINES-11, posX_map+9, 'x');

//fleche

for(int i=0;i<COLS;i++) {
for(int y=0;y<5;y++){ mvaddch(y, i, '%');mvaddch(LINES-(y+1), i, '%');}}

//gravité
if(compteur%4000 == 0) {if(mvinch(posY+1, posX)==' ') posY++;}

    
    // Gestion de commandes: directions etc 
   commande(maFleche,&ch,&tir,LINES,COLS,&sens,&aim,&posX,&posY,&sautOn,&saut,&first,&compteurFleche);

    //Sauts et
    deplacement(&compteur,&sens, &posX, &posY,&sautOn, &saut,&ndsaut);


 /* On affiche le curseur */
if(sens==4){mvaddch(posY-2, posX, 'd');mvaddch(posY-1, posX-2, '{');mvaddch(posY-1, posX-1, '-');mvaddch(posY-1, posX+1, '\\');}
if(sens==6){mvaddch(posY-2, posX, 'b');mvaddch(posY-1, posX+2, '}');mvaddch(posY-1, posX+1, '-');mvaddch(posY-1, posX-1, '/');}
    
mvaddch(posY-1, posX, '|'); 
mvaddch(posY, posX, 'n');

	tir_Fleche(maFleche,0);
	refresh();
	}

 
	/* Arrêt de ncurses */
	ncurses_stopper();
 
	return EXIT_SUCCESS;
}




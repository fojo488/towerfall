#include <time.h>
#include <stdlib.h>  
#include <stdio.h>  /* Pour EXIT_SUCCESS */
#include <unistd.h>
#include <sys/types.h>

#include "deplacements.h"
#include "init.h"
#include "menu.h"
#include "creamap.h"

int main() 
{
/**
 * Initialisation de la souris.
 */
ncurses_souris();
 
/**
 * Recupere la position x et y de la souris.
 * @param[out] x la position en x
 * @param[out] y la position en y
 * @param[out] bouton l'évenement associé au clic
 * @return OK si reussite
 */
int i=0;

while((i=menue())!=4){
refresh();
}

return 0;
}




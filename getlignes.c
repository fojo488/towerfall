
//gcc Projet_S2_0.1.c -lncurses -o exe


#include <time.h>
#include <stdlib.h>  
#include <stdio.h>  /* Pour EXIT_SUCCESS */
#include <ncurses.h>    /* Pour printw, attron, attroff, COLOR_PAIR, getch */
#include <unistd.h>
#include <sys/types.h>


/**
 * Initialisation de ncurses.
 */
void ncurses_initialiser() {
  initscr();	        /* Demarre le mode ncurses */
  cbreak();	        /* Pour les saisies clavier (desac. mise en buffer) */
  noecho();             /* Desactive l'affichage des caracteres saisis */
  keypad(stdscr, TRUE);	/* Active les touches specifiques */
  refresh();            /* Met a jour l'affichage */
  curs_set(FALSE);      /* Masque le curseur */
  nodelay(stdscr, TRUE); /*Rends getch non interruptif*/
}
 
/**
 * Fin de ncurses.
 */
void ncurses_stopper() {
  endwin();
}
 
/**
 * Initialisation des couleurs.
 */
void ncurses_couleurs() {
  /* Verification du support de la couleur */
  if(has_colors() == FALSE) {
    ncurses_stopper();
    fprintf(stderr, "Le terminal ne supporte pas les couleurs.\n");
    exit(EXIT_FAILURE);
  }
 
  /* Activation des couleurs */
  start_color();
 
  /* Definition de la palette */
  init_pair(1, COLOR_BLUE, COLOR_BLACK);
  init_pair(2, COLOR_RED, COLOR_BLACK);
  init_pair(3, COLOR_GREEN, COLOR_BLACK);
}
 
int main() {
  int ch, posX, posY, posX_map, posY_map, posX_f, posY_f, tir=0, compteur = 0, first=1, aim=0, sens=0;
	char object;
	time_t t;
  /* Initialisation de ncurses */
  ncurses_initialiser();
  /* Routine principale */

printw("%d lignes et %d colonnes",LINES,COLS);

  while((ch = getch()) != KEY_F(2)) {
  /* On efface le curseur */

 
  /* Arrêt de ncurses */
}
 ncurses_stopper();

  return EXIT_SUCCESS;
}


